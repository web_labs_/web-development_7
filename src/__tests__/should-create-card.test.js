import React from 'react';
import { render, waitFor, fireEvent } from '@testing-library/react';
import { it, expect, jest } from '@jest/globals';
import { Column } from '../components';
import { act } from 'react-dom/test-utils';

const mockOnAddCard = jest.fn((columnId, cardName) => {
  console.log(`Add card ${cardName} to columnId ${columnId}`);
});

it(`Column должен содержать форму для создания карточки
и кнопку для создания. По клику на кнопку -- создается карточка`, async () => {
  const NEW_CARD_NAME = 'New card';

  const { container, rerender, findByText } = render(
    <Column id="666" onAddCard={mockOnAddCard} />
  );

  const cardNameInput = container.querySelector('#create_card_input');
  const createCardButton = container.querySelector('#create_card_button');

  await act(async () => {
    fireEvent.change(cardNameInput, { target: { value: NEW_CARD_NAME } });
    fireEvent.click(createCardButton);
  });
  expect(mockOnAddCard.mock.calls.length).toBe(1);

  const createdCardName = mockOnAddCard.mock.calls[0][0];
  const newCard = { id: Date.now(), name: createdCardName };

  rerender(<Column cards={[newCard]} />);

  await waitFor(async () => {
    const newCardElement = await findByText(createdCardName);
    expect(newCardElement).toBeTruthy();
  });
});
